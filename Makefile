
MANGO_ROOT ?= /opt/mango
BOSP_PATH  ?= $(MANGO_ROOT)/bosp
BUILD_TYPE ?= Release
CPUS       ?= $(shell cat /proc/cpuinfo | grep "model name" | wc -l)

hn_header := $(MANGO_ROOT)/include/libhn/hn.h
hn_daemon := $(MANGO_ROOT)/bin/hn_daemon

libbbque_pms := $(BOSP_PATH)/lib/bbque/libbbque_pms.so
libbbque_tg  := $(BOSP_PATH)/lib/bbque/libbbque_tg.so
libmango-so  := $(MANGO_ROOT)/lib/libmango.so

PROJECT_NAME  := "MANGO Frameworks and Libraries"
SRCDIR := $(shell pwd)
OUTDIR ?= $(SRCDIR)/build

GIT_VERSION := `git log --oneline -1 | awk '{ print $1 }'`

DEPENDENCIES := doxygen autoconf automake libtoolize cmake \
	gvgen flex bison yacc gawk curl bc dialog

REQS := $(foreach exec,$(DEPENDENCIES),\
		$(if $(shell which $(exec)),\
		OK,\
		$(error "Required dependency $(exec) not found.")))

.PHONY: all libhn libgn libmango bosp toolchains sdk sdk-peak samples beer


all: bosp libmango samples
	@echo
	@echo "*======================================================================================"
	@echo "|                     .:: $(PROJECT_NAME) ::.                                         "
	@echo "|======================================================================================"
	@echo "| ProDesign tools .............: " $(CONFIG_PRODESIGN_PATH)
	@echo "| Build type ..................: " $(BUILD_TYPE)
	@echo "| Build jobs ..................: " $(CPUS)
	@echo "| Source directory ............: " $(SRCDIR)
	@echo "| Installation directory ......: " $(MANGO_ROOT)
	@echo "| BOSP installation directory  : " $(BOSP_PATH)
	@echo "| Version (GIT commit)         : " $(GIT_VERSION)
	@echo "*======================================================================================"
	@echo

-include Makefile.kconf

-include .config

version:
	@echo "Version: "$(GIT_VERSION)


ifdef CONFIG_MANGO_BUILD_DEBUG
BUILD_TYPE = "Debug"
endif
ifdef CONFIG_MANGO_ROOT_DIR
MANGO_ROOT = $(CONFIG_MANGO_ROOT_DIR)
endif

toolchain_peak := $(MANGO_ROOT)/usr/local/mipsel-unknown-gappeak
toolchain_nup  := $(MANGO_ROOT)/usr/local/llvm-nuplus/


ifdef CONFIG_LIBMANGO_GN   #=== GN emulation =======================================

lib_arch := $(MANGO_ROOT)/lib/libgn.so

sdk:
	@echo
	@echo "----------------------------------------------"
	@echo "                   SDK                        "
	@echo "----------------------------------------------"
	@echo ...nothing to be done for GN Emulation...:
	@echo
else                       #=== GN emulation =====================================

lib_arch := $(MANGO_ROOT)/lib/libhn.so

sdk: toolchains sdk-peak
	@echo
	@echo " SDK installed."
	@echo

sdk-peak:
	@echo
	@echo "----------------------------------------------"
	@echo "           SDK for PEAK processors           "
	@echo "----------------------------------------------"
	@echo
	@cd $(SRCDIR)/sdk-peak/src && \
		MANGO_ROOT=$(MANGO_ROOT) PEAK_COMPILER_PATH=$(toolchain_peak)/bin/ make && \
		MANGO_ROOT=$(MANGO_ROOT) PEAK_COMPILER_PATH=$(toolchain_peak)/bin/ make install


toolchains: toolchain-peak toolchain-nup
	@echo
	@echo " Toolchains installed."
	@echo

toolchain-peak: $(toolchain_peak)

$(toolchain_peak):
	@echo
	@echo "----------------------------------------------"
	@echo "              PEAK toolchain                  "
	@echo "----------------------------------------------"
	@cd $(MANGO_ROOT) && \
		tar -Jxvf $(SRCDIR)/toolchain-gappeak/mipsel-unknown-gappeak.txz

ifdef CONFIG_LIBMANGO_NUPLUS

toolchain-nup: $(toolchain_nup)

$(toolchain_nup): $(SRCDIR)/toolchain-nup
	@echo
	@echo "----------------------------------------------"
	@echo "              NU+ toolchain                  "
	@echo "----------------------------------------------"
	@cd $(SRCDIR)/toolchain-nup/compiler && \
		mkdir -p build/ && cd build/ && \
		cmake .. -DCMAKE_INSTALL_PREFIX=$(toolchain_nup) && \
		make -j$(CPUS) && \
		make install
	@cd $(SRCDIR)/toolchain-nup/ && \
		TOPDIR=$(SRCDIR)/toolchain-nup MANGO_ROOT=$(MANGO_ROOT) make

else

toolchain-nup:
	@echo
	@echo "----------------------------------------------"
	@echo "              NU+ toolchain                  "
	@echo "----------------------------------------------"
	@echo Skipping this toolchain...

endif #  CONFIG_LIBMANGO_NUPLUS

endif #  CONFIG_LIBMANGO_GN 


ifndef CONFIG_LIBMANGO_GN

$(lib_arch): libhn

$(hn_header): libhn $(hn_daemon)

$(hn_daemon): $(SRCDIR)/libhn/hn_daemon
	@echo
	@echo "----------------------------------------------"
	@echo " HN daemon                                    "
	@echo "----------------------------------------------"
	@cd $^ && instdir=$(MANGO_ROOT) prodesign_dir=$(CONFIG_PRODESIGN_PATH) \
		build_type=$(BUILD_TYPE) make autostart_tempmon=yes -j$(CPUS) && \
		instdir=$(MANGO_ROOT) make install

else     # ==== GN emulation =======================================================

$(lib_arch): libgn

$(hn_header): $(SRCDIR)/libhn/
	@echo
	@echo "----------------------------------------------"
	@echo " HN library headers                           "
	@echo "----------------------------------------------"
	@cd $(SRCDIR)/libhn && instdir=$(MANGO_ROOT) make install_headers

$(hn_daemon): $(SRCDIR)/libhn/hn_daemon
	@echo
	@echo "----------------------------------------------"
	@echo " HN daemon headers                            "
	@echo "----------------------------------------------"
	@cd $(SRCDIR)/libhn/hn_daemon && instdir=$(MANGO_ROOT) make install_headers

endif #  CONFIG_LIBMANGO_GN ======================================================


libhn: $(SRCDIR)/libhn/
	@echo
	@echo "----------------------------------------------"
	@echo " HN library                                   "
	@echo "----------------------------------------------"
	@cd $(SRCDIR)/libhn && instdir=$(MANGO_ROOT) \
		build_type=$(BUILD_TYPE) make -j$(CPUS) && \
		instdir=$(MANGO_ROOT) make install


libgn: $(hn_header) $(hn_daemon)
	@echo
	@echo "----------------------------------------------"
	@echo " GN library                                   "
	@echo "----------------------------------------------"
	@export CONFIG_LIBMANGO_GN
	@cd $(SRCDIR)/libgn && mkdir -p build && cd build \
		&& cmake .. -DBOSP_PATH=$(BOSP_PATH) \
			-DMANGO_ROOT=$(MANGO_ROOT) \
			-DBUILD_TYPE=$(BUILD_TYPE) \
		&& make -j$(CPUS) && sudo make install

bosp_build: $(lib_arch) $(hn_header)
	@echo
	@echo "----------------------------------------------"
	@echo "              BarbequeRTRM                    "
	@echo "----------------------------------------------"
	@cd $(SRCDIR)/bosp && make menuconfig && make -j$(CPUS)

$(SRCDIR)/bosp/out: bosp_build

bosp: $(SRCDIR)/bosp/out
	@echo ""
	@echo "----------------------------------------------"
	@echo "              BarbequeRTRM installation       "
	@echo "----------------------------------------------"
	@echo " From  $(SRCDIR)/bosp/out/	             "
	@echo " To    $(BOSP_PATH)                           "
	@echo "----------------------------------------------"
	@if [ ! -d $(BOSP_PATH) ]; then mkdir -p $(BOSP_PATH); fi
	@cp -r $(SRCDIR)/bosp/out/* $(BOSP_PATH)


$(libbbque_pms): bosp

$(libbbque_tg): bosp

$(libbbque_rtlib): bosp

$(libmango-so): libmango

ifdef CONFIG_LIBMANGO_GN
CONFIG_HHAL := "GN"
else
CONFIG_HHAL := "Mango"
endif

libmango: sdk beer $(lib_arch) $(libbbque_pms) $(libbbque_tg)
	@echo
	@echo "----------------------------------------------"
	@echo "              MANGO API library               "
	@echo "----------------------------------------------"
	@cd $(SRCDIR)/libmango && mkdir -p build && cd build \
		&& cmake .. -DBOSP_PATH=$(BOSP_PATH) \
			-DCONFIG_RM="BarbequeRTRM" \
			-DCMAKE_INSTALL_PREFIX=$(MANGO_ROOT)/usr \
			-DBUILD_TYPE=$(BUILD_TYPE) \
			-DCONFIG_HHAL=$(CONFIG_HHAL) \
		&& make -j$(CPUS) && make install

ifdef CONFIG_BEER_FRAMEWORK
beer: $(libbbque_rtlib)
	@echo
	@echo "----------------------------------------------"
	@echo "              BEER library                    "
	@echo "----------------------------------------------"
	@cd $(SRCDIR)/beer && mkdir -p build && cd build \
		&& cmake .. -DMANGO_ROOT=$(MANGO_ROOT) \
			-DCMAKE_INSTALL_PREFIX=$(MANGO_ROOT)/usr \
			-DCMAKE_BUILD_TYPE=$(BUILD_TYPE) \
		&& make -j$(CPUS) && make install
else
beer: $(libbbque_rtlib)
	@echo
	@echo "----------------------------------------------"
	@echo "              BEER library: disabled          "
	@echo "----------------------------------------------"
endif

# ==================== Clean targets ===============================

.PHONY: clean clean_libhn clean_libgn clean_bbque
.PHONY: clean_libmango clean_sdk-peak clean_hn-daemon clean_beer

clean_libhn:
	@echo "Cleaning libhn..."
	@cd libhn && make clean

clean_hn-daemon:
	@echo "Cleaning hn-daemon..."
	@cd libhn/hn_daemon && make clean

clean_libgn:
	@echo "Cleaning libgn..."
	@cd libgn && rm -rf build/

clean_bosp: clean_bbque
	@echo "Cleaning BOSP..."
	@cd bosp && make clean_out

clean_bbque:
	@cd bosp && make clean_bbque

clean_libmango:
	@echo "Cleaning libmango..."
	@cd libmango && if [ -d "build" ]; then rm -r build; fi

clean_beer:
	@echo "Cleaning beer..."
	@cd beer && if [ -d "build" ]; then rm -r build; fi

clean_sdk-peak:
	@echo "Cleaning PEAK SDK..."
	@cd $(SRCDIR)/sdk-peak/src && make clean

clean_toolchain-nup:
	@echo "Cleaning NU+ Toolchain..."
	@cd $(SRCDIR)/toolchain-nup/compiler/build &&  make clean && \
		rm -rf $(SRCDIR)/toolchain-nup/compiler/build
	@cd $(SRCDIR)/toolchain-nup/ &&  make clean


clean: clean_libhn clean_hn-daemon clean_libgn clean_bbque clean_beer clean_libmango clean_sdk-peak clean_samples 
	@echo "Cleaning..."
	@if [ -d "build" ]; then rm -r $(OUTDIR); fi
	@echo "> Done."


clean_config:
	@echo "Removing configuration files..."
	@rm -f .config $(cmake_config)

distclean: clean clean_config
	@cd $(SRCDIR)/bosp && make clean && make clean_out


# ==================== Samples targets ===============================

samples: # $(libmango-so)
	@echo
	@echo "----------------------------------------------"
	@echo "          MANGO Sample applications           "
	@echo "----------------------------------------------"
	@cd samples && mkdir -p build && cd build \
		&& cmake .. -DBOSP_PATH=$(MANGO_ROOT)/bosp \
			-DMANGO_ROOT=$(MANGO_ROOT) \
			-DCMAKE_PREFIX_PATH=$(MANGO_ROOT)/usr \
			-DBUILD_TYPE=$(BUILD_TYPE) \
		&& make && make install

clean_samples:
	@echo
	@echo "Cleaning sample applications..."
	@cd $(SRCDIR)/samples && if [ -d "build" ]; then rm -r build; fi
	@cd $(SRCDIR)/samples/nuplus_matrix_multiplication/kernel && if [ -d "obj" ]; then rm -r obj; fi

