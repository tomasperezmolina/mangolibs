# README #

EU H2020 MANGO Project: Runtime and libraries

### What is this repository for? ###

* This repository includes most of the components of the MANGO software stack
* Each component is managed as a self-contained submodule project

### How do I get set up? ###

* Setup the MANGO HW platform
* Clone this repository
* Download the sources of each submodule:

  $ ./setup.sh

* Compile and install all the libraries:

  $ MANGO_ROOT=/installation/path/ make

### How do I get updates? ###
* $ ./update.sh

### Contribution guidelines ###

* The issue must refer to a specific MANGO SW component
* The maintainer will assign the issue to a contributor
* The contributor will be responsible of the issue resolution
* The maintainer will update the issue status

### Who do I talk to? ###

* Repo owner: Giuseppe Massari 'Politecnico di Milano'
* http://www.mango-project.eu/

