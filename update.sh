#!/bin/bash

submodules=`ls -l | egrep '^d' | awk '{print $9}'`
echo "Project modules: " $submodules

branch=master
if [ "x$1" != "x" ]; then
	branch=$1
fi

echo "====== Fetching changes ===== "
git fetch origin --prune

printf "+++++ Updating root project branch [%s] +++++\n" $branch

git checkout -B $branch origin/$branch
if [ $? -ne 0 ]; then
	printf "Error: cannot retrieve branch [%s] from upstream\n" $branch
	exit 1
fi
git pull origin $branch --prune

echo "++++++ Updating sub-projects (modules) +++++ "
git submodule sync --recursive
git submodule update --init --recursive

echo "===== BOSP update configuration ====="
cd bosp && make boostrap

